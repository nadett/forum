<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/posts', 'HomeController@store')->name('posts.store');
Route::get('/posts/{post}', 'HomeController@show')->name('posts.show');
Route::get('/posts/{post}/edit', 'HomeController@edit')->name('posts.edit');
Route::patch('/post/{post}', 'HomeController@update')->name('posts.update');
