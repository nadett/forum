<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $posts = Post::orderBy('id', 'desc')->paginate(5);
//        $posts = Post::orderByDesc('id')->paginate(5);
        $posts = Post::latest()->paginate(5);

        return view('home', compact('posts'));
    }

    public function store(Request $request)
    {
        $request->validate([
//            'user_id' => [
//                'required',
//                'numeric',
//            ],
            'title'   => [
                'required',
            ],
            'post'    => [
                'required',
            ],
        ]);
//        $user_id = $request->input('user_id');
//        $title = $request->input('title');
//        $content = $request->input('post');

//        $post = new Post();

//        $post->create([
//            'user_id' => $user_id,
//            'title'   => $title,
//            'post'    => $content,
//        ]);

        $request->user()
            ->posts()
            ->create([
                'title' => $request->title,
                'post' => $request->post,
            ]);

        return redirect(route('home'));
    }

    public function edit(Post $post)
    {

//        $request->validate([
//            'post_id' => [
//                'numeric',
//                'required',
//            ],
//        ]);
//
//        $editable_post = Post::where('id', $request->input('post_id'))->get();

        return view('edit', compact('post'));
    }

    public function update(Post $post, Request $request)
    {
        $request->validate([
//            'user_id' => [
//                'required',
//                'numeric',
//            ],
//            'post_id' => [
//                'required',
//                'numeric',
//            ],
            'title'   => [
                'required',
            ],
            'post'    => [
                'required',
            ],
        ]);

//        Post::where('user_id', $request->input('user_id'))
//            ->where('id', $request->input('post_id'))
        $post->update([
                'title' => $request->input('title'),
                'post'  => $request->input('post'),
            ]);
        return redirect(route('home'));
    }

    public function show(Post $post)
    {
        return view('show', compact('post'));
    }
}
