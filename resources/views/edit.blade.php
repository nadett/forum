@extends('layouts.app')

@section('content')
    <form action="{{ route('posts.update', $post) }}" method="post">
        @csrf
        @method('patch')
        {{--<input type="hidden" name="user_id" value="{{ Auth::id() }}"/>--}}
        <div class="form-group">
            <label for="title">Title:</label>
            {{--<input type="hidden" value="{{ $post->id }}" name="post_id"/>--}}
            <input type="text" name="title" value="{{ old('title', $post->title) }}" class="form-control">
        </div>
        <div class="form-group">
            <label for="pwd">Content</label>
            <textarea type="textarea" name="post" rows="7" class="form-control">{{ $post->post }}</textarea>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
@endsection
