@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-5">
            <div class="form-area">
                <form role="form" method="post" action="{{ route('posts.store') }}">
                    @csrf
                    <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Write a post</h3>
{{--                    <input type="hidden" name="user_id" value="{{ Auth::id() }}">--}}
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" placeholder="Title"
                               value="{{ old('title') }}" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" type="textarea" name="post" placeholder="Content"
                                  rows="7"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Post it!
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @foreach($posts as $post)
            <div class="card">
                <div class="card-header bg-success"><span>{{ $post->title }}</span><b
                        style="float: right; size: 40px">{{ $post->user->name }}</b></div>
                <div class="card-body">
                    {{ $post->post }}
                    @if(Auth::id() == $post->user_id)
                        <a href="{{ route('posts.edit', $post) }}" class="btn btn-danger btn-small">
                            Szerkesztés
                        </a>
                    @endif
                </div>

            </div>
        @endforeach
    </div>
    <div class="row justify-content-center"> {{ $posts->onEachSide(2)->links() }}</div>

    <script src="{{ asset('js/app.js') }}"></script>

@endsection
